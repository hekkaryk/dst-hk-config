local hk_respawnable = Class(function(self, inst)
    self.inst = inst
	self.delay = 0
	self.shouldAdd = TheWorld.components.hk_respawnable_controller.shouldAdd
end)

function hk_respawnable:OnRemoveEntity()
	if self.shouldAdd then
		TheWorld.components.hk_respawnable_controller:AddPrefabRespawn(self.inst, self.delay)
	else
		TheWorld.components.hk_respawnable_controller:AddPrefabRespawnIndirect(self.inst, self.delay)
	end
end

function hk_respawnable:OnSave()
	return
	{
		delay = self.delay,
		shouldAdd = self.shouldAdd
	}
end

function hk_respawnable:OnLoad(data)
	if data then
		self.delay = data.delay
		self.shouldAdd = data.shouldAdd
	end
end

return hk_respawnable