local hk_respawnable_controller = Class(function(self, inst)
    self.inst = inst
    self.double_respawn_days = 0
    self.structure_exclusion_zone_size = 0
    self.prefabs_respawn_table = {}
    self.prefabs_respawn_table_indirect = {}
    self.currentDay = 0
    self.currentHour = 0
    self.shouldAdd = true

    self.restoreTurfs = false
    self.turfs = nil
    self.turfs_respawn_table = {}
    self.turfsRestoreDelay = 0

    self.inst:DoPeriodicTask(TUNING.SEG_TIME, function()
        self:ClockChanged()
    end)

    self.inst:DoTaskInTime(3, function()
        self:InitTurfs()
    end)
end)

function hk_respawnable_controller:AddPrefabRespawn(inst, respawndelay)
    print("[Hk]Config++ Info: hk_respawnable_controller:AddPrefabRespawn starts...")

    if not inst then
        print("[Hk]Config++ Warn: hk_respawnable_controller:AddPrefabRespawn - inst is nil")
        return
    end

    local x, y, z = inst.Transform:GetWorldPosition()

    if x ~= nil and y ~= nil and not (x == 0 and y == 0) then
        table.insert(self.prefabs_respawn_table[(self.currentDay + respawndelay) % self.double_respawn_days + 1],
            {inst.prefab, x, y, z})

        print("[Hk]Config++ Info: hk_respawnable_controller:AddPrefabRespawn done.")
    end
end

function hk_respawnable_controller:AddPrefabRespawnIndirect(inst, respawndelay)
    print("[Hk]Config++ Info: hk_respawnable_controller:AddPrefabRespawnIndirect starts...")

    if not inst then
        print("[Hk]Config++ Warn: hk_respawnable_controller:AddPrefabRespawnIndirect - inst is nil")
        return
    end

    local x, y, z = inst.Transform:GetWorldPosition()

    if x ~= nil and y ~= nil and not (x == 0 and y == 0) then
        table.insert(self.prefabs_respawn_table_indirect[(self.currentDay + respawndelay) % self.double_respawn_days + 1],
            {inst.prefab, x, y, z})

        print("[Hk]Config++ Info: hk_respawnable_controller:AddPrefabRespawnIndirect done.")
    end
end

function hk_respawnable_controller:SpawnInstance(inst)
    if type(inst) ~= "table" then
        print("[Hk]Config++ Error: failed to spawn instance since it wasn't a table")
        --return
    end

    local ents = TheSim:FindEntities(inst[2], inst[3], inst[4], self.structure_exclusion_zone_size, {"structure"}) -- 1=inst.prefab, 2=x, 3=y, 4=z

    for _, _ in ipairs(ents) do -- Don't respawn in the middle of a base
        return false
    end

    local newPrefab = SpawnPrefab(inst[1])
    if newPrefab then
        newPrefab.Transform:SetPosition(inst[2], inst[3], inst[4])
    end

    return true
end

function hk_respawnable_controller:AddTurfRespawn(x, y)
    if not self.restoreTurfs then
        return
    elseif self.turfs[x + 1] == nil or self.turfs[x + 1][y + 1] == nil then
        print("[Hk]Config++ Error: Failed to add turf respawn - world's InitTurfs failed?")
        return
    end

    print("[Hk]Config++ Info: hk_respawnable_controller:AddTurfRespawn(" .. x .. ", " .. y .. ")")

    for _, day in ipairs(self.turfs_respawn_table) do
        for j = #day, 1, -1 do
            if day[j][1] ~= nil and day[j][2] ~= nil and day[j][1] == x and day[j][2] == y then
                table.remove(day, j)
            end
        end
    end

    local targetDay = (self.currentDay + self.turfsRestoreDelay) % self.double_respawn_days + 1
    table.insert(self.turfs_respawn_table[targetDay], {x, y})
    print("[Hk]Config++ Info: hk_respawnable_controller:AddTurfRespawn(" .. x .. ", " .. y .. ") - added turf respawn for day " ..
              targetDay)
end

function hk_respawnable_controller:RestoreTurf(x, y)
    if not self.restoreTurfs then
        return
    end

    if type(self.turfs) ~= "table" then
        print("[Hk]Config++ Error: Malformed turf regen data, component defunct - turf regen won't work")
        return
    end

    if self.turfs[x + 1] == nil then
        print("[Hk]Config++ Warn: Skipping turf regen for row " .. x .. ": partially corrupted turf regen data")
        return
    end

    local tile = self.turfs[x + 1][y + 1]
    if tile == nil then
        print("[Hk]Config++ Warn: Skipping turf regen for tile " .. x .. ", " .. y .. ": missing tile regen data")
        return
    end

    -- This does not work!
    -- print("[Hk]Config++ DEBUG: printing ipairs(GROUND)")
    -- for name,value in ipairs(GROUND) do
    --     print("[Hk]Config++ DEBUG: name: " .. name .. " value: " .. value)
    --     if name == tile then
    --        ground_match_found = true
    --     end
    -- end

    local found = false

    for name in pairs(GROUND) do
        if tonumber(tile) == tonumber(GROUND[name]) then
            found = true
            break
        end
    end

    if found == false then
        print("[Hk]Config++ Error: Tile type " .. tile .. " not found - check for disabled mods!")
        return
    end

    local map = TheWorld.Map
    local minimap = TheWorld.minimap.MiniMap
    local original_tile_type = map:GetTile(x, y)

    if original_tile_type ~= GROUND.DIRT then
        return
    end

    map:SetTile(x, y, tile)
    map:RebuildLayer(original_tile_type, x, y)
    map:RebuildLayer(tile, x, y)

    minimap:RebuildLayer(original_tile_type, x, y)
    minimap:RebuildLayer(tile, x, y)

    print("[Hk]Config++ Info: Turf " .. tile .. " regenerated at " .. x .. ", " .. y)
end

function hk_respawnable_controller:ClockChanged()
    print("[Hk]Config++ Info: hk_respawnable_controller:ClockChanged - prefabs scheduled for respawn:")

    for k, v in ipairs(self.prefabs_respawn_table) do
        if k == self.currentDay + 1 then
            print("day: " .. k .. " prefabs: " .. #v .. " (today)")
        else
            print("day: " .. k .. " prefabs: " .. #v)
        end
    end

    self.shouldAdd = false
    self.currentHour = self.currentHour + 1

    if self.currentHour >= 16 then
        self.currentHour = 0
        self.currentDay = self.currentDay + 1

        if self.currentDay >= self.double_respawn_days then
            self.currentDay = 0
        end
    end

    local prefabs_today = self.prefabs_respawn_table[self.currentDay + 1]

    for _ = 1, #prefabs_today / (16 - self.currentHour) do
        self.shouldAdd = true
        self:SpawnInstance(prefabs_today[1])
        table.remove(prefabs_today, 1)
        self.shouldAdd = false
    end

    for day, entries in ipairs(self.prefabs_respawn_table_indirect) do
        for entry in pairs(entries) do
            table.insert(self.prefabs_respawn_table[day], entries[entry])
        end
        self.prefabs_respawn_table_indirect[day] = {}
    end

    if self.restoreTurfs then
        print("[Hk]Config++ Info: hk_respawnable_controller:ClockChanged - turfs scheduled for respawn:")

        for k, v in ipairs(self.turfs_respawn_table) do
            if k == self.currentDay + 1 then
                print("day: " .. k .. " turfs: " .. #v .. " (today)")
            else
                print("day: " .. k .. " turfs: " .. #v)
            end
        end

        local turfs_today = self.turfs_respawn_table[self.currentDay + 1]

        for _ = 1, #turfs_today / (16 - self.currentHour) do
            local turf = turfs_today[1]

            if turf ~= nil and turf[1] ~= nil and turf[2] ~= nil then
                self:RestoreTurf(turf[1], turf[2])
                table.remove(turfs_today, 1)
            end
        end
    end
end

function hk_respawnable_controller:Configure(speed, respawn_turfs, exclusion_zone_size)
    print("[Hk]Config++ Info: hk_respawnable_controller:Configure(" .. speed .. ", " .. tostring(respawn_turfs) .. ", " ..
              exclusion_zone_size .. ")")

    self.structure_exclusion_zone_size = exclusion_zone_size
    self.restoreTurfs = respawn_turfs
    self.double_respawn_days = speed * 2
    self.turfsRestoreDelay = speed * 2 - 1
    self.prefabs_respawn_table = {}
    self.turfs_respawn_table = {}

    for _ = 1, self.double_respawn_days do
        table.insert(self.prefabs_respawn_table, {})
        table.insert(self.prefabs_respawn_table_indirect, {})
        table.insert(self.turfs_respawn_table, {})
    end
end

function hk_respawnable_controller:InitTurfs()
    print("[Hk]Config++ Info: Analyzing world turf metadata...")

    if not self.restoreTurfs or self.turfs ~= nil then
        return
    end

    if TheWorld == nil or TheWorld.Map == nil then
        print("[Hk]Config++ Fatal: World map metadata not available")
        return
    end

    local temp = {}
    local world = TheWorld
    local map = world.Map
    local maxx, maxy = map:GetSize()

    for x = 1, maxx do
        local row = {}

        for y = 1, maxy do
            row[y] = map:GetTile(x - 1, y - 1)
        end

        temp[x] = row
    end

    print("[Hk]Config++ Info: Finished analysing world turf metadata - #turfs: " .. #temp)

    self.turfs = temp
end

function hk_respawnable_controller:CheckTurfs(turfs)
    if TheWorld == nil or TheWorld.Map == nil then
        print("[Hk]Config++ Fatal: World map metadata not available")
        return
    end

    if type(turfs) ~= "table" then
        print("[Hk]Config++ Fatal: Saved world map metadata is useless, map tile check type not possible")
        return
    end

    local maxx, maxy = TheWorld.Map:GetSize()
    local unknowntiles = {}

    for x, world_row in pairs(turfs) do
        if type(x) ~= "number" then
            print("[Hk]Config++ Warn: Saved world map row is useless (non-number row number)")
        elseif x < 0 or x > maxx then
            print("[Hk]Config++ Warn: Saved world map row is out-of-bounds!")
        elseif type(world_row) ~= "table" then
            print("[Hk]Config++ Warn: Saved world map row has no columns")
        else
            for _, tile in pairs(world_row) do
                if type(x) ~= "number" then
                    print("[Hk]Config++ Warn: Saved world map tile is useless (non-number tile number)")
                elseif x < 0 or x > maxy then
                    print("[Hk]Config++ Warn: Saved world map tile is out-of-bounds!")
                elseif type(tile) ~= "number" then
                    print("[Hk]Config++ Warn: Saved world map tile stores no tile number")
                else
                    local found = false
                    for name in pairs(GROUND) do
                        if GROUND[name] == tile then
                            found = true
                            break
                        end
                    end

                    if found == false then
                        unknowntiles[tile] = true
                    end
                end
            end
        end
    end

    if next(unknowntiles) ~= nil then
        local s = nil
        for tile, _ in pairs(unknowntiles) do
            if s == nil then
                s = tostring(tile)
            else
                s = s .. "," .. tostring(tile)
            end
        end

        print(
            "[Hk]Config++ Warn: Unknown turfs present in saved metadata; respawning turfs might fail (please check enabled mods!) - unknown types: " ..
                s)
    end
end

function hk_respawnable_controller:OnSave()
    self:tprint(self.prefabs_respawn_table, 1)

    local data = {
        shouldAdd = self.shouldAdd,
        currentHour = self.currentHour,
        currentDay = self.currentDay,
        prefabs_respawn_table = self.prefabs_respawn_table,
        prefabs_respawn_table_indirect = self.prefabs_respawn_table_indirect,
        structure_exclusion_zone_size = self.structure_exclusion_zone_size
    }

    if self.restoreTurfs then
        data.turfs = self.turfs
        data.turfs_respawn_table = self.turfs_respawn_table

        self:tprint(self.turfs_respawn_table, 1)
    end

    return data
end

function hk_respawnable_controller:OnLoad(data)
    if data then
        self.currentHour = data.currentHour
        self.currentDay = data.currentDay
        self.structure_exclusion_zone_size = data.structure_exclusion_zone_size

        -- Fixes a crash when regeneration interval is lowered too much (less than current day/2). Fix by Muche.
        if self.currentDay >= self.double_respawn_days then
            self.currentDay = 0
        end

        if data and data.prefabs_respawn_table then
            self.prefabs_respawn_table = data.prefabs_respawn_table
        else
            print("[Hk]Config++ Warn: data.prefabs_respawn_table nil, repairing...")
            self.prefabs_respawn_table = {}
        end
        if data and data.prefabs_respawn_table_indirect then
            self.prefabs_respawn_table_indirect = data.prefabs_respawn_table_indirect
        else
            print("[Hk]Config++ Warn: data.prefabs_respawn_table_indirect nil, repairing...")
            self.prefabs_respawn_table_indirect = {}
        end
        if data and data.shouldAdd then
            self.shouldAdd = data.shouldAdd
        else
            print("[Hk]Config++ Warn: data.shouldAdd nil, repairing...")
            self.shouldAdd = true
        end
        for _ = 1, #self.prefabs_respawn_table - self.double_respawn_days do
            for j = 1, #self.prefabs_respawn_table[self.double_respawn_days + 1] do
                table.insert(self.prefabs_respawn_table[self.double_respawn_days],
                    self.prefabs_respawn_table[self.double_respawn_days + 1][j])
            end
            table.remove(self.prefabs_respawn_table, self.double_respawn_days + 1)
        end

        for _ = 1, self.double_respawn_days - #self.prefabs_respawn_table do -- In case renew delay has been increased make table big enough
            table.insert(self.prefabs_respawn_table, {})
        end

        for _ = 1, #self.prefabs_respawn_table_indirect - self.double_respawn_days do
            for j = 1, #self.prefabs_respawn_table_indirect[self.double_respawn_days + 1] do
                table.insert(self.prefabs_respawn_table_indirect[self.double_respawn_days],
                    self.prefabs_respawn_table_indirect[self.double_respawn_days + 1][j])
            end
            table.remove(self.prefabs_respawn_table_indirect, self.double_respawn_days + 1)
        end

        for _ = 1, self.double_respawn_days - #self.prefabs_respawn_table_indirect do -- In case renew delay has been increased make table big enough
            table.insert(self.prefabs_respawn_table_indirect, {})
        end

        if self.restoreTurfs then
            if data.turfs ~= nil then
                self:CheckTurfs(data.turfs)
                self.turfs = data.turfs
            end

            if data.turfs_respawn_table ~= nil then
                self.turfs_respawn_table = data.turfs_respawn_table
            end

            for _ = 1, #self.turfs_respawn_table - self.double_respawn_days do
                for j = 1, #self.turfs_respawn_table[self.double_respawn_days + 1] do
                    table.insert(self.turfs_respawn_table[self.double_respawn_days],
                        self.turfs_respawn_table[self.double_respawn_days + 1][j])
                end
                table.remove(self.turfs_respawn_table, self.double_respawn_days + 1)
            end

            for _ = 1, self.double_respawn_days - #self.turfs_respawn_table do -- In case renew delay has been increased make table big enough
                table.insert(self.turfs_respawn_table, {})
            end
        end
    end

    self:tprint(self.prefabs_respawn_table, 1)

    if self.restoreTurfs then
        self:tprint(self.turfs_respawn_table, 1)
    end
end

-- Analyse table structure
function hk_respawnable_controller:tprint(tbl, indent)
    for k, v in pairs(tbl) do
        local formatting = string.rep("  ", indent) .. k .. ": "
        if type(v) == "table" then
            print(formatting)
            self:tprint(v, indent + 1)
        else
            print(formatting .. v)
        end
    end
end

return hk_respawnable_controller
