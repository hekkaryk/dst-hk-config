AddPrefabPostInit(
    "world",
    function(TheWorld)
        TheWorld:DoTaskInTime(
            0,
            function(TheWorld)
                for k, v in pairs(_G.SPECIAL_EVENTS) do
                    if v ~= _G.SPECIAL_EVENTS.NONE then
                        local tech = _TECH[k]
                        if tech ~= nil then
                            _TECH[k].SCIENCE = 0
                        end
                    end
                end
                function _G.IsSpecialEventActive(event)
                    return true
                end
            end
        )
    end
)
