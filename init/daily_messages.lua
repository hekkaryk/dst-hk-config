if config.enable_daily_messages ~= nil and config.enable_daily_messages > 0 then
    local current_message_number = 1
    
    function daily_message()
        local level_name = is_cave() and "Caves" or "Surface"
    
        --announce("" .. level_name .. " will be cleaned " .. format_time(warning_time) .. "!")
        --log_l(level_name .. " will be cleaned " .. format_time(warning_time))
        if _G.TheWorld.hk_config_daily_message_task ~= nil then
            _G.TheWorld.hk_config_daily_message_task:Cancel()
            _G.TheWorld.hk_config_daily_message_task = nil
        end

        

        _G.TheWorld.hk_config_daily_message_task = _G.TheWorld:DoTaskInTime(config.enable_daily_messages, daily_message)
    end  
    
    local function add_daily_message_to_world(world)
        if not ismastersim() then
            return
        end
    
        world.hk_config_daily_message_task = world:DoTaskInTime(config.enable_daily_messages, daily_message)
    end
    
    AddPrefabPostInit("forest", add_daily_message_to_world)
    AddPrefabPostInit("cave", add_daily_message_to_world)
end