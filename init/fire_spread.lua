if config.no_fire_spread then
    local function SetPropagateRangeToZero(inst)
        if inst.components.propagator then
            inst.components.propagator.propagaterange = 0
        end
    end
    local CurrentMakeSmallPropagator = _G.MakeSmallPropagator
    _G.MakeSmallPropagator = function(inst)
        CurrentMakeSmallPropagator(inst)
        SetPropagateRangeToZero(inst)
    end
    local CurrentMakeMediumPropagator = _G.MakeMediumPropagator
    _G.MakeMediumPropagator = function(inst)
        CurrentMakeMediumPropagator(inst)
        SetPropagateRangeToZero(inst)
    end
    local MakeLargePropagator = _G.MakeLargePropagator
    _G.MakeLargePropagator = function(inst)
        MakeLargePropagator(inst)
        SetPropagateRangeToZero(inst)
    end
end
