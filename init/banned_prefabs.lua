local function remove_post_init(inst)
    inst:Remove()
end

local banned_recipe_ingredients = {
    _Ingredient("yellowgem", 10000),
    _Ingredient("greengem", 10000),
    _Ingredient("orangegem", 10000)
}
local banned_recipe_tab = _RECIPETABS.ORPHANAGE
local banned_recipe_tech = _TECH.LOST

for prefab in string.gmatch(config.banned_prefabs, "([^,^%s]+)") do
    AddRecipe(prefab, banned_recipe_ingredients, banned_recipe_tab, banned_recipe_tech)

    AddPrefabPostInit(prefab, remove_post_init)
end