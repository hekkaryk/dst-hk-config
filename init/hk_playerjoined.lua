local player_joined = function(_, player) -- src, player)
    if config.fastbuilder and not player:HasTag("fastbuilder") then
        player:AddTag("fastbuilder")
        log_i("Perk fastbuilder added as part of regular login.")
    end

    for v in string.gmatch(config.banned_characters, "([^,^%s]+)") do
        if v == player.prefab then
            log_i("Despawning player " .. player.name .. " because they played as " .. player.prefab ..
                      " which is a banned character.")

            player:DoTaskInTime(config.banned_characters_popup_delay, function()
                say(player, config.banned_characters_message)
                player:DoTaskInTime(config.banned_characters_timeout, function()
                    if config.banned_character_item_drop then
                        inventory_drop_everything(player)
                    end
                    despawn_player(player)
                end)
            end)
            break
        else
            log_d(player.prefab .. " does not match " .. v)
        end
    end

    if config.enable_commands and config.enable_rank_badges then
        regenerate_players_ranks_json()
    end
end

local function ListenForPlayers(inst)
    if ismastersim() then
        inst:ListenForEvent("ms_playerjoined", player_joined, inst)
    end
end
--AddPrefabPostInit("world", ListenForPlayers)

AddComponentPostInit("playerspawner", function(OnPlayerJoin, inst)
    ListenForPlayers(inst)
end)