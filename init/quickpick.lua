if config.quick_pick then
    local function quickpick(inst)
        if inst.components.pickable then
            inst.components.pickable.quickpick = true
        end
    end
    AddPrefabPostInit("berrybush", quickpick)
    AddPrefabPostInit("berrybush2", quickpick)
    AddPrefabPostInit("blue_mushroom", quickpick)
    AddPrefabPostInit("cactus", quickpick)
    AddPrefabPostInit("flower_cave", quickpick)
    AddPrefabPostInit("flower_cave_double", quickpick)
    AddPrefabPostInit("flower_cave_triple", quickpick)
    AddPrefabPostInit("grass", quickpick)
    AddPrefabPostInit("green_mushroom", quickpick)
    AddPrefabPostInit("lichen", quickpick)
    AddPrefabPostInit("marsh_bush", quickpick)
    AddPrefabPostInit("red_mushroom", quickpick)
    AddPrefabPostInit("reeds", quickpick)
    AddPrefabPostInit("sapling", quickpick)
    AddPrefabPostInit("cave_banana_tree", quickpick)
    AddPrefabPostInit("oasis_cactus", quickpick)
    AddPrefabPostInit("wormlight_plant", quickpick)
    AddPrefabPostInit("marsh_bush", quickpick)
    -- AddPrefabPostInit("mushroom_farm", quickpick)
    -- AddPrefabPostInit("slow_farmplot", quickpick)
    -- AddPrefabPostInit("fast_farmplot", quickpick)
    -- Multi-World DST part of quickpick
    if loaded_mods["Multi-Worlds DST"] then
        AddPrefabPostInit("coffeebush", quickpick)
        AddPrefabPostInit("bittersweetbush", quickpick)
        AddPrefabPostInit("mintybush", quickpick)
        AddPrefabPostInit("bambootree", quickpick)
        AddPrefabPostInit("rock_limpet", quickpick)
        log_i("Multi-World DST mod quick pick added.")
    else
        log_i("Multi-World DST mod quick pick skipped.")
    end
    if
        loaded_mods["My Birds and Berries [Megarandom]"] or
            loaded_mods["Birds and Berries and Trees and Flowers for Friends"]
     then
        AddPrefabPostInit("appletree", quickpick)
        AddPrefabPostInit("berryblu2", quickpick)
        AddPrefabPostInit("berryblue", quickpick)
        AddPrefabPostInit("berrygre2", quickpick)
        AddPrefabPostInit("berrygree", quickpick)
        AddPrefabPostInit("pineapple", quickpick)
        AddPrefabPostInit("treeapple", quickpick)
        log_i("Birds and Berries and Trees and Flowers for Friends mod quick pick added.")
    else
        log_i("Birds and Berries and Trees and Flowers for Friends mod quick pick skipped.")
    end
    if loaded_mods["DST Advanced Farming"] then
        AddPrefabPostInit("hybrid_banana_tree", quickpick) 
        log_i("DST Advanced Farming mod quick pick added.")
    else
        log_i("DST Advanced Farming mod quick pick skipped.")
    end
    if loaded_mods["Wind Goddess magic"] then
        AddPrefabPostInit("greengrass", quickpick)
        log_i("Wind Goddess magic mod quick pick added.")
    else
        log_i("Wind Goddess magic mod quick pick skipped.")
    end
    if loaded_mods["Tea and Trees"] then
        AddPrefabPostInit("tea_plant", quickpick)
        log_i("Tea and Trees mod quick pick added.")
    else
        log_i("Tea and Trees mod quick pick skipped.")
    end
    if loaded_mods["Red Lycoris(support DS&DST)"] then

        log_i("Red Lycoris(support DS&DST) mod quick pick added.")
    else
        log_i("Red Lycoris(support DS&DST) mod quick pick skipped.")
    end
end
