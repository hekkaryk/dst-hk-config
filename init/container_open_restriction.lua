AddComponentPostInit("container", function(inst)
    inst.CurrentOpen = inst.Open
    function inst:Open(doer)
        if inst.opener == nil and doer then
            local canOpen = true
            if self.inst.prefab == "chester" then
                if doer.components.inventory == nil then -- Just in case
                    canOpen = false
                else
                    local eyebone = doer.components.inventory:GetItemByName("chester_eyebone", 1)
                    canOpen = false
                    for k, v in pairs(eyebone) do
                        if k.chesterTag and self.inst.chesterTag == k.chesterTag then
                            canOpen = true
                        end
                    end
                end
            elseif self.inst.prefab == "dragonflychest" and
                (not doer.components.builder or not doer.components.builder:KnowsRecipe("dragonflychest")) then
                canOpen = false -- The builder does not know the dragonfly chest recipe, so it is certain he did not build this
            elseif self.inst.prefab == "icebox" and
                (not doer.components.builder or not doer.components.builder:KnowsRecipe("icebox")) then
                canOpen = false -- The builder does not know the icebox recipe, so it is certain he did not build this
            elseif self.inst.prefab == "treasurechest" and
                (not doer.components.builder or not doer.components.builder:KnowsRecipe("treasurechest")) then
                canOpen = false -- The builder does not know the treasure chest recipe, so it is certain he did not build this
            end

            local age = (doer.components and doer.components.age) and doer.components.age:GetAgeInDays() or 0

            if age >= config.minimum_days_to_open_container then -- Is old enough, so rest doesn't matter
                canOpen = true
            end
            if canOpen then -- There is no (certain) reason why he shouldn't be allowed to open the chest
                return self:CurrentOpen(doer)
            elseif doer.components.talker then
                say(doer, "It's locked (play for " .. config.minimum_days_to_open_container - age ..
                    " more days to unlock opening any containers).")
            end
        end
    end
end)
