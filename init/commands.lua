-- Hello there ^_^
if config.enable_rank_badges then
    json = GLOBAL.json
end
local rank_names = {}
local rank_values = {}

-- Commands component. Commands are executed by typing in command prefixed by command marker
if IsServer then -- we CANNOT execute commands client-side
    -- cache is used to prevent constant file access, in order to reduce disk IO
    local cache_players = {}
    local cache_ranks = {}
    cache_ranks[""] = {}
    local cache_invites = {}

    local rank_index = 1

    if not_nil_or_empty(config.ranks) then
        local rank_number = 0

        for name in first_level_string_split(config.ranks) do
            if not not_nil_or_empty(name) then
                log_w(
                    "malformatted ranks format - encountered more than one space, comma or semicolon in a row - ignoring and attempting to continue")
            else
                log_d("Rank read: " .. name)
                table.insert(rank_names, name)

                if commands_filenames[name] == nil then
                   commands_filenames[name] = "hk_config_rank_" .. rank_number
                end
            end

            rank_number = rank_number + 1
        end
    end

    for name in pairs(rank_names) do
        rank_values[rank_names[name]] = rank_index
        log_d("New rank: " .. rank_names[name] .. " index: " .. rank_index)
        rank_index = rank_index + 1
    end

    function remember_player(player)
        if not cache_players[player.userid] then
            local old_file = io.open(commands_filenames.players, "rb")
            local found = false
            local players_list = ""
            if old_file then
                for line in old_file:lines() do
                    local fixed_line = line:gsub("\r", "")
                    local parts = {}
                    for part in string.gmatch(fixed_line, "%S+") do
                        table.insert(parts, part)
                    end
                    if not cache_players[parts[1]] then
                        cache_players[parts[1]] = {}
                        cache_players[parts[1]].name =
                            parts[2] -- might as well load entire player cache since we're at it
                            -- might as well load entire player cache since we're at it
                        -- might as well load entire player cache since we're at it
                    end
                    if line == (player.userid .. " " .. player.name) then
                        found = true
                        break
                    else
                        players_list = players_list .. line .. "\n"
                    end
                end
                old_file:close()
            end
            if not found then
                local file = io.open(commands_filenames.players, "w")
                players_list = players_list .. player.userid .. " " .. player.name .. "\n"
                file:write(players_list)
                file:close()
                cache_players[player.userid] = {}
                cache_players[player.userid].name = player.name
            end
        end
    end

    function load_players_cache()
        local file = io.open(commands_filenames.players, "rb")
        if file then
            for line in file:lines() do
                local fixed_line = line:gsub("\r", "")
                local parts = {}
                for part in string.gmatch(fixed_line, "%S+") do
                    table.insert(parts, part)
                end
                if not cache_players[parts[1]] then
                    cache_players[parts[1]] = {}
                    cache_players[parts[1]].name = parts[2]
                end
            end
        end
---@diagnostic disable-next-line: undefined-field
        for _, player in pairs(_G.AllPlayers) do
            if not cache_players[player.userid] or not cache_players[player.userid].name then
                cache_players[player.userid] = {}
                cache_players[player.userid].name = player.name
                remember_player(player)
            end
        end
    end

    -- returns a string formed into (userid) name (or at least name)
    function identity(player, by_userid)
        local message = ""
        if not by_userid then
            if player and player.userid and player.userid then
                message = "(" .. player.userid .. ") " .. player.name
            elseif player then
                message = "" .. player -- we want failsafe here to make sure string is returned

            else
                log_event("Warning",
                    "Identity caller is gone! This should happen only when listing players in rank but some of them are not in-game, in chache or player_list.txt")
            end
        else
            local first_try = true
            while first_try do
                if cache_players and cache_players[player] and cache_players[player].name then
                    message = "(" .. player .. ") " .. cache_players[player].name
                else
                    load_players_cache()
                    first_try = false
                end
            end
        end
        return message
    end

    -- allows searching via character name
    local character_names = {
        wilson = "wilson",
        willow = "willow",
        wolfgang = "wolfgang",
        wendy = "wendy",
        wx78 = "wx78",
        wickerbottom = "wickerbottom",
        woodie = "woodie",
        wes = "wes",
        wigfrid = "wathgrithr",
        webber = "webber"
    }

    -- finds player by userid
    function get_player(userid)
---@diagnostic disable-next-line: undefined-field
        for _, v in ipairs(_G.AllPlayers) do
            if v ~= nil and v.userid and v.userid == userid then
                return v
            end
        end
        return nil
    end

    -- searches for player by character name or player name
    function find_player(player, user, by_userid)
        if user == nil then
            return nil
        end
        if not by_userid then
            user = string.lower(user)
        end
        if not (not by_userid and player ~= nil and character_names[user] ~= nil) then -- search by player name
            -- _G.UserToClient(user)
            local multiplematches = false
            local target = nil

---@diagnostic disable-next-line: undefined-field
            for _, testplayer in pairs(_G.AllPlayers) do
                if not by_userid and string.contains(string.lower(testplayer.name), user) or by_userid and
                    testplayer.userid == user then
                    if target ~= nil then
                        target = nil
                        multiplematches = true
                        break
                    else
                        target = testplayer
                    end
                end
            end

            if multiplematches then
                log_event("Log", "Multiple users matched '" .. user .. "'")
            else
                if target ~= nil then
                    return target
                else
                    log_event("Log", "Unable to find user '" .. user .. "'")
                end
            end

            return nil
        else -- search by character name, not player name
            if player == nil then
                return nil
            end

            local dist = nil
            local target = nil

            for _, testplayer in pairs(_G.AllPlayers) do
                if testplayer.userid ~= player.userid and testplayer.prefab == character_names[user] then
                    local testdist = player:GetDistanceSqToInst(testplayer)
                    if dist == nil or testdist < dist then -- first match is just saved - next are saved only if they are closer to player
                        dist = testdist
                        target = testplayer
                    end
                end
            end
            return target
        end
    end

    function find_player_userid(player, name)
        if player and name then
            local result = nil
            local multiplematches = false
            -- in currently logged in players
            for _, candidate in pairs(_G.AllPlayers) do
                if string.contains(string.lower(candidate.name), name) then
                    if result ~= nil then
                        result = nil
                        multiplematches = true
                        break
                    else
                        result = candidate.userid
                    end
                end
            end
            if not multiplematches and not result then
                for k, v in pairs(cache_players) do
                    if v.name and v.name == name then
                        if result ~= nil then
                            result = nil
                            multiplematches = true
                            break
                        else
                            result = k
                        end
                    end
                end
            end
            if not multiplematches and not result then
                load_players_cache()
                for k, v in pairs(cache_players) do
                    if v.name and v.name == name then
                        if result ~= nil then
                            result = nil
                            multiplematches = true
                            break
                        else
                            result = k
                        end
                    end
                end
            end
            if multiplematches or not result then
---@diagnostic disable-next-line: undefined-field
                result = _G.UserToClient(name)
                result = result.userid
            end
            if multiplematches then
                say(player, "There was more than one player that matched " .. name)
            elseif not result then
                say(player, "Could not find player with name " .. name)
            end
            return result
        end
    end

    -- true if player is admin
    function is_admin(player)
---@diagnostic disable-next-line: undefined-field
        for _, v in pairs(_G.TheNet:GetClientTable()) do
            if (v.userid == player.userid and v.admin) then
                return true
            end
        end
        return false
    end

    -- true if player is dead
    function is_dead(player)
        if player and player:HasTag("player") and player:HasTag("playerghost") then
            return true
        end
    end

    function is_invisible(inst)
        local _, _, _, a = inst.AnimState:GetMultColour()
        return a == 0
    end

    -- stores temporary teleportation data - session only
    local marked_points_forest = {}
    local marked_points_caves = {}
    local temp_points_forest = {}
    local temp_points_caves = {}

    -- sets return coordinates
    function set_return(userid, x, y, z, pull)
        if is_cave() then
            if pull then
                temp_points_caves[userid] = {x, y, z}
            else
                marked_points_caves[userid] = {x, y, z}
            end
        else
            if pull then
                temp_points_forest[userid] = {x, y, z}
            else
                marked_points_forest[userid] = {x, y, z}
            end
        end
    end

    -- gets return coordinates
    function get_return(userid, pull)
        local location = nil
        if is_cave() then
            if pull then
                location = temp_points_caves[userid]
            else
                location = marked_points_caves[userid]
            end
        else
            if pull then
                location = temp_points_forest[userid]
            else
                location = marked_points_forest[userid]
            end
        end
        if location ~= nil then
            return location[1], location[2], location[3]
        end
    end

    -- stores portal coordinates
    local portal_command_targets = {}
    if config.enable_cmd_portal then
        for v in string.gmatch(config.cmd_portal_target, "([^,^%s]+)") do
            AddPrefabPostInit(v, function(inst)
                table.insert(portal_command_targets, inst.GUID)
            end)
        end
    end

    function get_players_in_rank(player, rank)
        local valid_rank_name = false
        local invalid_name_message = "Rank name invalid - try: "
        for _, v in pairs(rank_names) do
            invalid_name_message = invalid_name_message .. v .. " "
            if v == rank then
                valid_rank_name = true
                break
            end
        end
        if valid_rank_name then
            local file = io.open(commands_filenames[rank], "rb")
            local message = ""
            if file then
                message = "Players in rank " .. rank .. ": "
                for line in file:lines() do
                    local userid = line:gsub("\r", "")
                    local id_string = identity(find_player(player, userid, true))
                    if id_string then
                        message = message .. id_string .. ", "
                    elseif cache_players[line] and cache_players[line].name then
                        message =
                            (message .. "(" .. line .. ") " .. cache_players[line].name .. ", ") -- it crashed here!
                            -- it crashed here!
                    else
                        message = message .. "(" .. line .. "), "
                    end
                end
            else
                message = "Could not load file " .. commands_filenames[rank]
            end
            say(player, message)
        else
            say(player, invalid_name_message)
        end
    end

    function files_check()
        local result = false
        for _, name in pairs(commands_filenames) do
            if not file_check(name) then
                if not file_create(name) then
                    log_event("Error", "failed to create file " .. name)
                else
                    log_event("Info", "file " .. name .. " was missing")
                    result = true
                end
            else
                result = true
            end
        end
        return result
    end

    -- handling line ends the proper yet complex way
    -- from: https://stackoverflow.com/questions/20598937/handling-cr-line-endings-in-lua
    -- function cr_lines(s)
    --	 return s:gsub('\r\n?', '\n'):gmatch('(.-)\n')
    -- end
    -- function cr_file_lines(filename)
    --	 local f = io.open(filename, 'rb')
    --	 local s = f:read('*a')
    --	 f:close()
    --	 return cr_lines(s)
    -- end

    function is_player_rank(player, rank, report_status)
        -- local userid_length = string.len(player.userid)
        local result = false
        if not cache_ranks[rank] then
            cache_ranks[rank] = {}
            log_e("Ranks cache is nil")
        end
        if cache_ranks[rank][player.userid] then
            result = true
            log_d("Player " .. player.userid .. " is " .. rank .. " according to cache")
        else
            -- for line in cr_file_lines(commands_filenames[rank]) do
            --	 if line == player.userid then
            --		 print(line .."==".. player.userid)
            --	 else
            --		 print(line .."~=".. player.userid)
            --	 end
            -- end

            local file_status = file_check(commands_filenames[rank])
            if file_status then
                local file = io.open(commands_filenames[rank], "rb")
                if file then
                    for line in file:lines() do
                        local userid = line:gsub("\r", "")
                        -- string.len(line) - userid_length == 1 and string.sub(line, 1, userid_length) or line
                        cache_ranks[rank][userid] = true
                        if userid == player.userid then
                            log_d("Player " .. player.userid .. " is " .. rank .. " according to file")
                            result = true
                        end
                    end
                else
                    log_event("Error", "Failed to open file " .. commands_filenames[rank])
                end
                if file then
                    file:close()
                end
            elseif report_status then
                say(player, rank .. " file not found")
            else
                log_event("Error", "No file " .. commands_filenames[rank])
            end
        end

        if not result then
            log_d("Player " .. player.userid .. " is not " .. rank .. " according to file")
        end

        return result
    end

    function player_rank_check(player, minimum_rank)
        -- admins can everything, always
        local result = is_admin(player)

        if result == false and minimum_rank < rank_index then
            for i = minimum_rank, rank_index-1, 1 do
                result = is_player_rank(player, rank_names[i])
                if result then
                    break
                end
            end
        end

        log_d("Player: " .. player.name .. "[" .. player.userid .. "] Required: " .. minimum_rank .. "result: " .. ( result and "andresult" or "orresult"))

        return result
    end

    function change_rank(player, rank, promote, target) -- if promote == false player is demoted
        if player == nil then
            log_e("Command-calling player is gone!")
        elseif rank == nil then
            say(player, "You forgot to specify rank")
        elseif target == nil then
            say(player, "Player not found")
        elseif not rank_values[rank] or rank == "admin" then
            local s_rank_names = ""
            for i in pairs(rank_names) do
                s_rank_names = s_rank_names .. rank_names[i] .. " "
            end
            say(player, "Invalid rank specified. Options are: " .. s_rank_names)
        else
            if not cache_ranks[rank] then
                cache_ranks[rank] = {}
            end
            if promote then
                cache_ranks[rank][target.userid] = true
            else
                cache_ranks[rank][target.userid] = false
            end
            local file_status = file_check(commands_filenames[rank])
            local file_lines = ""
            if not file_status then
                say(player, "Failed to open file" .. commands_filenames[rank])
            else
                local found = false
                local file0 = io.open(commands_filenames[rank], "rb")
                if file0 then
                    for line in file0:lines() do
                        local userid = line:gsub("\r", "")
                        if userid == target.userid then
                            found = true
                            if promote then
                                log_event("Log", "Player " .. identity(target) .. " is already in rank " .. rank)
                            end
                            break
                        else
                            file_lines = file_lines .. "\n" .. userid
                        end
                    end
                    file0:close()
                    if not found and promote then -- adding player to rank
                        file_lines = file_lines .. "\n" .. target.userid
                        local file = io.open(commands_filenames[rank], "w")
                        file:write(file_lines)
                        file:close()
                        say(player, "Added player " .. target.name .. " to: " .. rank)
                    elseif found and not promote then -- removing player from rank
                        local file = io.open(commands_filenames[rank], "w")
                        file:write(file_lines)
                        file:close()
                        say(player, "Removed player " .. target.name .. " from: " .. rank)
                    end
                else
                    say(player, "Failed to open file " .. commands_filenames[rank])
                end
            end
        end
        -- return result
    end

    function player_ban(player, target, ban)
---@diagnostic disable-next-line: undefined-field
        local blacklist = _G.TheNet:GetBlacklist()
        local new_blacklist = {}
        if ban then
            if not is_admin(target) then
                if not target.hasleft then
                    GLOBAL.TheNet:Ban(target.userid)
                else
                    table.insert(blacklist, {
                        steamname = "Unknown",
                        steamid = "Unknown",
                        servername = "Unknown",
                        serverdescription = "Unknown",
                        date = "Unknown",
                        userid = target.userid,
                        timestamp = "Unknown",
                        character = "Unknown"
                    })
                    GLOBAL.TheNet:SetBlacklist(blacklist)
                end
                log_event("Ban", identity(target) .. " has been banned by " .. identity(player))
            else
                say(player, "You cannot ban an admin")
                log_event("Ban", identity(player) .. " tried to ban " .. identity(target))
            end
        else
            for _, banned in pairs(blacklist) do
                if banned.userid ~= target then
                    table.insert(new_blacklist, banned)
                end
            end
            GLOBAL.TheNet:SetBlacklist(new_blacklist)
            log_event("Unban", target .. " has been unbanned.")
        end
    end

    function extinguish(x, y, z)
        if x ~= nil and y ~= nil and z ~= nil then
---@diagnostic disable-next-line: undefined-field
            local ents = _G.TheSim:FindEntities(x, y, z, 30)
            for _, ent in ipairs(ents) do
                if ent.components ~= nil and ent.components.burnable ~= nil and ent.components.burnable.Extinguish ~=
                    nil then
                    if not ent:HasTag("campfire") and ent.prefab ~= "nightlight" then
                        ent.components.burnable:Extinguish()
                    end
                end
            end
        end
    end

    function player_freeze(player, freeze)
        if freeze then
            extinguish(player.Transform:GetWorldPosition())
            player.taskstasis = player:DoPeriodicTask(1, function()
                if player.components.grogginess then
                    player.components.grogginess:KnockOut()
                else
                    player:PushEvent("knockedout")
                end
                log_event("Stasis:On", identity(player) .. " continues state of suspended animation.")
            end)
        else
            player.components.health:SetInvincible(false)
            log_event("Stasis:Off", identity(player) .. " leaves state of suspended animation.")
            if player.stasistask ~= nil then
                player.stasistask:Cancel()
                player.stasistask = nil
            end
            player:CancelAllPendingTasks()
            if player.components.grogginess then
                player.components.grogginess:ComeTo()
            else
                log_event("Stasis:Error", identity(player) .. " could not leave state of suspended animation!")
            end
        end
    end

    function player_godmode(player, heal)
        if player then
            local godmode = player.components.health.invincible
            player.components.health:SetInvincible(not godmode)

            if not godmode and heal then
                player.components.health:SetPercent(1)
                player.components.hunger:SetPercent(1)
                player.components.sanity:SetPercent(1)
                player.components.moisture:SetPercent(0)
                player.components.temperature:SetTemperature(25)
                say(player, ";)")
            end
            return godmode
        end
    end

    -- http://lua-users.org/wiki/StringRecipes
    function string.starts(String, Start)
        return string.sub(String, 1, string.len(Start)) == Start
    end
    -- http://lua-users.org/wiki/StringRecipes
    function string.ends(String, End)
        return End == "" or string.sub(String, -string.len(End)) == End
    end
    -- verified using http://lua-users.org/wiki/StringLibraryTutorial string.find(s, pattern [, index [, plain]])
    function string.contains(self, substring)
        return string.find(self, substring, 1, true) ~= nil
    end

    function get_access_level(player)
        if not IsServer then
            return
        end
        local access_level = 0
        if is_admin(player) then
            access_level = rank_index
        else
            for i = rank_index, 1, -1 do
                if player_rank_check(player, i) then
                    access_level = i
                    break
                end
            end
        end
        return access_level
    end

    function command_help(player)
        local temp = "All commands starts with: /\nEnabled commands:"
        temp = config.enable_cmd_revive and temp .. "/revive" or temp
        temp = config.enable_cmd_portal and temp .. " /portal" or temp
        temp = config.enable_cmd_return and temp .. " /mark /return" or temp

        temp = config.enable_cmd_tp and temp .. " /tp PLAYER" or temp
        temp = config.enable_cmd_ext and temp .. " /ext" or temp
        temp = config.enable_cmd_save and temp .. " /save /load /delete\nCoordinates per rank: " ..
                   config.coords_per_rank or temp
        say(player, temp)
    end

    function command_info(player, name)
        local target
        if not name then
            target = player
        else
            target = find_player(player, name)
        end
        if target then
            local message = "Player " .. target.name .. " is:"
            message = is_admin(target) and message .. " Administrator" or message
            for i = 1, rank_index-1, 1 do
                message = is_player_rank(target, rank_names[i]) and message .. " " .. rank_names[i] or message
            end
            say(player, message)
        else
            say(player, "Player " .. name .. " not found")
        end
    end

    function select_target(player, name)
        if not name then
            return player
        else
            local target = find_player(player, name)
            if target == nil then
                say(player, "Player " .. name .. " not found")
            end
            return target
        end
    end

    function command_revive(player, source, name)
        local target = select_target(player, name)
        if target and target:IsValid() and is_dead(target) then
            target:PushEvent("respawnfromghost")
            if config.command_revive_penalty > 0 then
                if target.components and target.components.health and target.components.health.penalty then
                    target.components.health.penalty = math.clamp(
                                                           target.components.health.penalty +
                                                               config.command_revive_penalty, 0, 0.75)
                    log_event("DEBUG", "Applying revive penalty: " .. config.command_revive_penalty ..
                        " max penalty == " .. _TUNING.MAXIMUM_HEALTH_PENALTY)
                else
                    log_event("DEBUG", "Cannot apply revive penalty - player misses required component!")
                end
            end
            target.rezsource = source or "Magic"
        end
    end

    function player_restore_stat(player, stats, name, target0)
        local target = target0 or select_target(player, name)
        if string.contains(stats, "health") then
            if target and target.components and target.components.health and target.components.health.penalty then
                target.components.health:SetPenalty(0)
                target.components.health:SetPercent(1)
                target.components.health:ForceUpdateHUD()
            end
        end
        if string.contains(stats, "sanity") then
            if target and target.components and target.components.sanity then
                target.components.sanity:SetPercent(1)
            end
        end
        if string.contains(stats, "hunger") then
            if target and target.components and target.components.hunger then
                target.components.hunger:SetPercent(1)
            end
        end
    end

    function command_kill(player, murder, name)
        if not murder then
            player:PushEvent("death")
            log_event("Log", identity(player) .. " commits suicide via command die")
        elseif name then
            local target = find_player(player, name)
            if target then
                if not player_rank_check(target, 3) then
                    target:PushEvent("death")
                    log_event("Log", identity(player) .. " kills player " .. identity(target) .. " using command kill")
                else
                    say(player, "You can't kill Moderators and above!")
                    say(target, player.name .. " tried to kill you via kill command!")
                    log_event("Log",
                        identity(player) .. " tries to kill player " .. identity(target) .. " using command kill")
                end
            end
        else
            say(player, "Specify who should die.")
        end
    end

    local last_targets = {}

    local function teleport_to_portal_target(player)
        local found = false

        for _, v in pairs(portal_command_targets) do
            if _G.Ents[v] then
                local first = false

                if #portal_command_targets == 1 or last_targets[player.userid] == nil then
                    last_targets[player.userid] = {}
                    first = true
                    log_d("first try")
                end

                local okay = true
                if not first then
                    for _, prev in pairs(last_targets[player.userid]) do
                        if prev == v then
                            okay = false
                            log_d("target: " .. v .. " is invalid. Current previous targets count: " ..
                                      #last_targets[player.userid])
                            break
                        end
                    end
                end

                if first or okay then
                    player.Transform:SetPosition(_G.Ents[v].Transform:GetWorldPosition())
                    table.insert(last_targets[player.userid], v)
                    found = true
                    if first then
                        log_d("first")
                    end
                    if okay then
                        log_d("okay")
                    end
                    log_d("found valid target: " .. v .. " current previous targets count: " ..
                              #last_targets[player.userid])
                    break
                end
            end
        end

        return found
    end

    function command_portal(player)
        log_d("valid targets count: " .. #portal_command_targets)
        local found = teleport_to_portal_target(player)

        if not found then
            log_d("valid portal target not found in first try")
            last_targets[player.userid] = nil
            found = teleport_to_portal_target(player)
        end

        if not found then
            say(player, "Portal not found")
        end
    end

    function command_mark(player, speak, pull)
        local x, y, z = player.Transform:GetWorldPosition()
        set_return(player.userid, x, y, z, pull)
        if speak then
            say(player, "Return point marked - use /return (or /b) to return here")
        end
    end

    function command_return(player, speak)
        local x, y, z = get_return(player.userid)
        if x ~= nil and y ~= nil and z ~= nil then
            player.Transform:SetPosition(x, y, z)
        elseif speak then
            say(player, "No point marked to /return to")
        end
    end

    function search_condition(k, name, type)
        if type == 0 then
            return k == name
        elseif type == 1 then
            return string.starts(k, name)
        elseif type == 2 then
            return string.starts(k, string.lower(name))
        end
    end

    local function coords_list(player, level, speak)
        local player_coords_at_level = {}
        local player_coords_message = "Saved coordinates:\n"
        if player.hkdata == nil or player.hkdata.coords == nil or player.hkdata.coords[level] == nil then
            player_coords_message = "You have no saved coords."
            log_d("There was missing primary coords data.")
        else
            if type(player.hkdata.coords[level]) ~= "table" then
                player.hkdata.coords[level] = {}
                log_d("Resetting coords table...")
            end

            player_coords_at_level = player.hkdata.coords[level]

            for key, _ in pairs(player.hkdata.coords[level]) do
                player_coords_message = player_coords_message .. key .. "\n"
            end
        end
        if speak then
            say(player, player_coords_message)
        end
        return player_coords_at_level, player_coords_message
    end

    local function coords_save(player, level, access_level, name)
        if player.hkdata == nil then
            log_e("Player hkdata is nil")
        end

        local coords_per_rank = config.coords_per_rank

        if coords_per_rank == -1 then
            say(player, "Saving coordinates is disabled.")
        else
            local player_coords, coords_message = coords_list(player, level, false)
            local owned = player_coords and #player_coords or 0

            if coords_per_rank ~= 0 and access_level < 4 and owned >= coords_per_rank + access_level * coords_per_rank then
                say(player, "You've reached your limit of coordinates - try deleting some of:\n" .. coords_message)
            else
                local x, y, z = player.Transform:GetWorldPosition()
                local existed_before = false
                local ok, error = GLOBAL.pcall(function()
                    existed_before = player_coords[name] and true or false
                end)
                if not ok then
                    existed_before = false
                    log_e("Listing error: " .. error)
                end
                if player.hkdata.coords == nil then
                    player.hkdata.coords = {}
                    log_d("Re-created player.hkdata.coords")
                end
                if player.hkdata.coords[level] == nil then
                    player.hkdata.coords[level] = {}
                    log_d("Re-created player.hkdata.coords.level")
                end

                player.hkdata.coords[level][name] = {}
                player.hkdata.coords[level][name]["x"] = x
                player.hkdata.coords[level][name]["y"] = y
                player.hkdata.coords[level][name]["z"] = z

                log_d("Player " .. identity(player) .. " saved new coordinates: " .. name .. " (" .. x .. " " .. y ..
                          " " .. z .. ")")

                if existed_before then
                    say(player, "Updated " .. name .. " (" .. x .. " " .. y .. " " .. z .. ")")
                else
                    say(player, "Saved " .. name .. " (" .. x .. " " .. y .. " " .. z .. ")")
                end
            end
        end
    end

    local function seach_condition(k, name, type)
        if type == 0 then
            return k == name
        elseif type == 1 then
            return string.starts(k, name)
        elseif type == 2 then
            return string.starts(k, string.lower(name))
        end
    end

    local function search_and_load(player, table, name, type)
        for key, _ in pairs(table) do
            if seach_condition(key, name, type) then
                if table[key]["x"] and table[key]["y"] and table[key]["z"] then
                    player.Transform:SetPosition(table[key]["x"], table[key]["y"], table[key]["z"])
                else
                    say(player, name .. " data is damaged - you can remove it with /delete " .. name)
                end
                return true
            end
        end
    end

    local function coords_load(player, level, name)
        local found = false
        local player_coords, coords_message = coords_list(player, level, false)
        for i = 1, 3, 1 do
            found = search_and_load(player, player_coords, name, i)
            if found then
                break
            end
        end
        if not found and name and coords_message then
            say(player, "Destination " .. name .. " not found\n" .. coords_message)
        end
    end

    local function coords_delete(player, level, name, everything)
        if everything then
            player.hkdata.coords = {}
        else
            local player_coords, coords_message = coords_list(player, level, false)
            local found = false
            local any_present = false
            for k, v in pairs(player_coords) do
                any_present = true
                if k == name then
                    player.hkdata.coords[level][k] = nil
                    say(player, "Deleted " .. k)
                    return
                end
            end
            if not any_present then
                say(player, "Nothing to delete, you have no coords saved.")
            elseif any_present and not found then
                say(player, "There are no coordinates called " .. name .. "\n" .. coords_message)
            end
        end
    end

    function command_fast_travel(player, operation, name)
        local ok, error = GLOBAL.pcall(function()
            local access_level = get_access_level(player) or 0
            local level = _G.TheWorld:HasTag("cave") and 1 or 0
            if operation == 0 then
                if not name then
                    say(player, "You forgot to specify name! 1 will do")
                elseif name and string.len(name) > 25 then
                    say(player, "Specified name must be 25 character max")
                elseif name then
                    coords_save(player, level, access_level, name)
                end
            elseif operation == 1 then
                if name == nil then
                    coords_list(player, level, true)
                else
                    coords_load(player, level, name)
                end
            elseif operation == 2 then
                if not name then
                    say(player, "You forgot to specify name!")
                else
                    coords_delete(player, level, name)
                end
            elseif operation == 3 then
                coords_delete(player, level, nil, true)
            end
        end)

        if not ok then
            say(player, "Couldn't fast travel due to black magic...")
            log_e("Fast travel error: " .. cast_to_string(error))
        end
    end

    function invite_file_modify(player, target, action)
        local file_status = file_check(commands_filenames.invites)
        local file_lines = ""
        if not file_status then
            say(player, "Failed to open file" .. commands_filenames.invites)
        else
            local found = false
            local file = io.open(commands_filenames.invites, "rb")
            if file then
                for line in file:lines() do
                    local fixed_line = line:gsub("\r", "")
                    local parts = {}
                    for part in string.gmatch(fixed_line, "%S+") do
                        table.insert(parts, part)
                    end
                    if action == 0 and parts[1] == player.userid and parts[2] == target.userid then
                        found = true
                        log_l("Player " .. identity(target) .. " is already allowed to visit " .. identity(player))
                        break
                    elseif action == 1 and parts[1] == player.userid and parts[2] == target.userid then
                        found = true
                    elseif action == 2 and parts[1] == player.userid then
                        found = true
                    else
                        file_lines = file_lines .. parts[1] .. " " .. parts[2] .. "\n"
                    end
                end
                file:close()
                if action == 0 and not found then
                    file_lines = file_lines .. player.userid .. " " .. target.userid .. "\n"
                    local file = io.open(commands_filenames.invites, "w")
                    file:write(file_lines)
                    file:close()
                elseif found and action > 0 then
                    local file = io.open(commands_filenames.invites, "w")
                    file:write(file_lines)
                    file:close()
                end
            else
                say(player, "Failed to open file " .. commands_filenames.invites)
            end
        end
    end

    function invites_file_read(player)
        local file_status = file_check(commands_filenames.invites)
        if not file_status then
            say(player, "Failed to open file" .. commands_filenames.invites)
        else
            local file = io.open(commands_filenames.invites, "rb")
            if file then
                for line in file:lines() do
                    local fixed_line = line:gsub("\r", "")
                    local parts = {}
                    for part in string.gmatch(fixed_line, "%S+") do
                        table.insert(parts, part)
                    end
                    if not cache_invites[player.userid] then
                        cache_invites[player.userid] = {}
                    end
                    if parts[1] == player.userid then
                        cache_invites[player.userid][parts[2]] = 1
                    end
                end
                file:close()
            else
                say(player, "Failed to open file " .. commands_filenames.invites)
            end
        end
    end

    function invite_add(player, target)
        if not cache_invites[player.userid] then
            cache_invites[player.userid] = {}
        end
        cache_invites[player.userid][target.userid] = 1

        invite_file_modify(player, target, 0)

        say(target, player.name .. " invited you - you can type .join " .. player.name .. " to teleport to them")
    end

    function command_tp(player, name)
        if name then
            local target = find_player(player, name)
            if target then
                local x, y, z = target.Transform:GetWorldPosition()
                player.Transform:SetPosition(x, y, z)
                log_event("Log", identity(player) .. " teleports to " .. identity(target))
            else
                say(player, "Player " .. name .. " not found")
            end
        else
            local first = true
            local message = "Select target:\n"
            for _, candidate in pairs(_G.AllPlayers) do
                if candidate.userid ~= player.userid then
                    if first then
                        first = false
                        message = message .. candidate.name
                    else
                        message = message .. ", " .. candidate.name
                    end
                end
            end
            if first then
                message = "No targets found"
            end
            say(player, message)
        end
    end

    function invite_join(player, target)
        if not cache_invites[target.userid] then
            invites_file_read(target)
        end
        if cache_invites[target.userid] and cache_invites[target.userid][player.userid] == 1 then
            command_tp(player, target.name)
        else
            say(player, "Ask " .. target.name .. " to .invite you first")
        end
    end

    function invite_deny(player, target)
        if cache_invites[player.userid] and cache_invites[player.userid][target.userid] then
            cache_invites[player.userid][target.userid] = 0
        end

        invite_file_modify(player, target, 1)

        say(player, "Revoked rights to visit you for " .. target.name)
    end

    function invite_deny_name(player, name)
        if cache_invites[player.userid] then
            local target_userid = find_player_userid(player, name)
            if target_userid then
                invite_file_modify(player, target_userid, 1)

                if cache_invites[player.userid] and cache_invites[player.userid][target_userid] and
                    cache_invites[player.userid][target_userid] == 1 then
                    cache_invites[player.userid][target_userid] = -1
                    say(player, name .. " can't visit you now.")
                end
            else
                say(player, "Could not find player matching name " .. name)
            end
        end
    end

    function invite_denyall(player)
        if cache_invites[player.userid] then
            cache_invites[player.userid] = nil
        end
        invite_file_modify(player, nil, 2)
        say(player, "Nobody can visit you now.")
    end

    function command_invite(player, action, name)
        if name or action == 4 then
            local target = find_player(player, name)
            if (target and target.userid) or action > 2 then
                if action == 0 then
                    invite_add(player, target)
                elseif action == 1 then
                    invite_join(player, target)
                elseif action == 2 then
                    invite_deny(player, target)
                elseif action == 3 then
                    invite_deny_name(player, name)
                elseif action == 4 then
                    invite_denyall(player)
                end
            else
                say(player, "Could not find " .. name)
            end
        else
            say(player, "Who tho?")
        end
    end

    function command_ext(player)
        local x, y, z = player.Transform:GetWorldPosition()
        extinguish(x, y, z)
    end

    function command_stasis(player)
        local is_hibernating = player_godmode(player, false)
        player_freeze(player, is_hibernating)
    end

    function str_to_int_1(str, default)
        local int = nil
        if str then
            int = _G.tonumber(str)
        end
        if int == nil or int < 1 then
            return default or 1
        else
            return default and (int < default and int or default) or int
        end
    end

    function command_spawn_prefab(player, give, prefab, quantity_str, step_count_str, step_delay_str)
        if prefab then
            local to_spawn = str_to_int_1(quantity_str)
            local step_count = str_to_int_1(step_count_str, config.cmd_step_prefabs)
            local step_delay = str_to_int_1(step_delay_str, config.cmd_step_seconds)

            local entity = _G.SpawnPrefab(prefab)
            if entity ~= nil then
                local can_give = false
                entity:Remove()
                if give then
                    if entity.components and entity.components.inventoryitem and
                        entity.components.inventoryitem.canbepickedup then
                        can_give = true
                    else
                        say(player, 'Prefab "' .. prefab .. '" is not marked as pickable.')
                    end
                end

                if not can_give and give then
                    say(player, "Can't give " .. prefab .. " - try spawn command instead")
                else
                    local amount = 0
                    local step = 0
                    local delay = 0
                    while to_spawn > 0 do
                        amount = to_spawn > step_count and step_count or to_spawn
                        log_d("Giving: " .. amount .. " of " .. prefab .. " out of max " .. step_count ..
                                  " prefabs given every " .. step_delay .. " seconds after " .. delay .. " seconds")
                        player:DoTaskInTime(delay, function()
                            if player and give ~= nil and prefab and amount then
                                local entity
                                for _ = 1, amount, 1 do
                                    entity = _G.SpawnPrefab(prefab)
                                    if give then
                                        if entity and player.components and player.components.inventory then
                                            player.components.inventory:GiveItem(entity)
                                        end
                                    elseif entity then
                                        entity.Transform:SetPosition(player.Transform:GetWorldPosition())
                                    end
                                end
                            else
                                log_event("Error", "Task internal failure. Failed to read amount to give.")
                            end
                        end)
                        step = step + 1
                        delay = delay + step_delay
                        to_spawn = to_spawn - amount
                        if amount == 0 then
                            log_event("Error", "Spawn command shortwired, see debug")
                            break
                        end
                    end
                end
            else
                say(player, 'Prefab "' .. prefab .. '" not found.')
            end
        else
            log_e("WHATTHEFUCK! If you see this in log then Klei command API is broken AF!")
        end
    end

    function command_teleport(player, x, y, z)
        if x and y and z then
            player.Transform:SetPosition(x, y, z)
        else
            say(player, "One of the coordinates is missing")
        end
    end

    function command_rank(player, rank, operation, name)
        local promote = operation == "add" and true or false
        if rank and operation and name then
            if not is_admin(player) then
                say(player, "Only admins can set ranks!")
            else
                local target = find_player(player, name)
                change_rank(player, rank, promote, target)
                if config.enable_rank_badges then
                    regenerate_players_ranks_json()
                end
            end
        elseif rank and operation then
            change_rank(player, rank, promote, player)
            if config.enable_rank_badges then
                regenerate_players_ranks_json()
            end
        elseif rank then
            get_players_in_rank(player, rank)
        else
            say(player, "Try rank " .. config.ranks .. " [add|del] [player_name]")
        end
    end

    function command_reviveall()
---@diagnostic disable-next-line: undefined-field
        for _, player in pairs(_G.AllPlayers) do
            command_revive(player, "Mass Resurrection")
        end
    end

    function command_pull(player, name, push)
        local target = find_player(player, name)
        if target then
            if not push then
                local x, y, z = player.Transform:GetWorldPosition()
                if x and y and z then
                    target.Transform:SetPosition(x, y, z)
                else
                    log_e("Malformed pull coordinates!")
                end
                log_event("Log", identity(player) .. " teleports to himself " .. identity(target))
            else
                local x, y, z = get_return(target.userid, true)
                if x and y and z then
                    target.Transform:SetPosition(x, y, z)
                else
                    log_e("Malformed push coordinates!")
                end
                log_event("Log", identity(player) .. " pushes from himself " .. identity(target))
            end
        else
            say(player, "Player " .. name .. " not found")
        end
    end

    function command_freeze(player, freeze, name)
        local target = find_player(player, name)
        if target then
            player_freeze(target, freeze)
            log_event("Log", identity(player) .. " freezes " .. identity(target))
        elseif name then
            say(player, "Player " .. name .. " not found")
        else
            say(player, "You forgot to specify who do you want to unfreeze!")
        end
    end

    function command_hide(player, hide)
        local invisible = hide or not is_invisible(player)
        local color = invisible and 0 or 1
        player.AnimState:SetMultColour(color, color, color, color)
        player.DynamicShadow:Enable(not invisible)
        player.MiniMapEntity:SetEnabled(not invisible)
        if invisible then
            player:AddTag("noplayerindicator")
            player:Hide()
        else
            player:RemoveTag("noplayerindicator")
            player:Show()
        end
    end

    function command_kick(player, name)
        if name then
            local target = find_player(player, name)
            if target then
                inventory_drop_everything(target)

                GLOBAL.TheNet:Kick(target.userid)
                log_event("Log", identity(player) .. " kicks " .. identity(target))
            else
                say(player, "Player " .. name .. " not found")
            end
        else
            say(player, "You forgot to specify target player")
        end
    end

    function command_drop(player, name, speak)
        if name then
            local target = find_player(player, name)
            if target then
                inventory_drop_everything(target)

                log_event("Log", identity(player) .. " forces " .. identity(target) .. " to drop everything")
            elseif speak then
                say(player, "Player " .. name .. " not found")
            end
        elseif speak then
            say(player, "You forgot to specify target player")
        end
    end

    function command_filecheck(player)
        local file_status = files_check()
        if file_status == false then
            say(player, "Creating new files...")
            files_check()
        else
            say(player, "All required files exists :)")
        end
    end

    function command_ban(player, name, ban)
        if name then
            local operation = ban and " bans " or " unbans "
            if ban then
                local target = find_player(player, name)
                if target then
                    player_ban(player, target, ban)
                    log_event("Log", identity(player) .. operation .. identity(target))
                else
                    say(player, "Player " .. name .. " not found")
                end
            else
                local target_userid = find_player_userid(player, name)
                if target_userid then
                    player_ban(player, target_userid, ban)
                end
            end
        end
    end

    function command_reset(player, all, filename)
        if not all and not filename then
            say(player,
                "Please select file you would like to reset: players, mod, elite, prestige, coords_master_private, coords_master_public, coords_caves_private, coords_caves_public")
        elseif not all and filename then
            if commands_filenames[filename] then
                local file = io.open(commands_filenames[filename], "w")
                if file then
                    file:close()
                else
                    say(player, "Could not reset file")
                end
            else
                say(player,
                    "File unrecognized. What would like to reset: players, mod, elite, prestige, coords_master_private, coords_master_public, coords_caves_private, coords_caves_public?")
            end
        elseif all then
            for _, v in pairs(commands_filenames) do
                if v then
                    local file = io.open(v, "w")
                    if file then
                        file:write("")
                        file:close()
                    end
                end
            end
        end
    end

    function toggle_creative_mode(player, name)
        log_d("About to apply creative to " .. player.name .. "[" .. player.userid .. "] - name: " .. (name == nil and ">>nil<<" or name))
        local target = player

        if not (name == nil or name == "") then
            target = find_player(player, name)
        end

        if target ~= nil and target.components and target.components.builder then
            target.components.builder:GiveAllRecipes()
            log_d("Applying creative to " .. player.name .. "[" .. player.userid .. "]")
        end
    end

    function add_points(player, name, how_many)
        local ok, err = _G.pcall(function()
            local target = find_player(player, name)

            if target == nil then
                say(player, "Couldn't find " .. name)
            else
                local ok, err = _G.pcall(function()
                    local current = target.components.allachivcoin.coinamount
                    target.components.allachivcoin.coinamount = current + how_many
                end)
                if ok then
                    say(player, how_many .. " points given to " .. target.name)
                    say(target, "Received " .. how_many .. " achievements points from " .. player.name)
                else
                    say(player, "Couldn't give points to " .. target.name)
                    log_e(err == nil and "" or err)
                end
            end
        end)

        if not ok then
            log_e(err)
        end
    end

    function verify_access(player, required_access_level, silent)
        local player_access_level = get_access_level(player) or 0
        local result = false
        if required_access_level then
            if required_access_level == rank_index + 1 then
                if not silent then
                    say(player, "This command is disabled")
                end
            elseif player_access_level >= required_access_level then
                result = true
            else
                if not silent then
                    local or_above = required_access_level == rank_index and "" or " or above"
                    say(player, "You cannot use this command. You must be " .. rank_names[required_access_level] .. or_above)
                end
            end
        end
        return result
    end
end
-- THIS IS END OF SERVER-SIDE ONLY LOGIC
-- Shared logic starts here
local param_string = "param"
local param_count = 10
local command_spaced_params = {}
local command_spaced_paramsoptional = {}

for i = 1, 10, 1 do
    command_spaced_params[i] = param_string .. i
    command_spaced_paramsoptional[i] = true
end

local function merge_spaced_params(params_table, skip_first)
    local result = nil
    local first = false
    local first_index = skip_first ~= true and 1 or 2
    for i = first_index, param_count, 1 do
        if params_table[param_string .. i] ~= nil then
            if first == false then
                result = "" .. params_table[param_string .. i]
                first = true
            else
                result = result .. " " .. params_table[param_string .. i]
            end
        end
    end
    return result
end

local function merge_spaced_params_or_nil(params_table, skip_first)
    if params_table == nil then
        return nil
    end

    return merge_spaced_params(params_table, skip_first)
end

function cmd_data(aliases, serverfn, params, paramsoptional)
    return {
        aliases = (aliases),
        prettyname = nil,
        desc = nil,
---@diagnostic disable-next-line: undefined-field
        permission = _G.COMMAND_PERMISSION.USER,
        slash = false,
        usermenu = false,
        servermenu = false,
        params = params or {},
        paramsoptional = paramsoptional or nil,
        vote = false,
        serverfn = serverfn
    }
end

function serverfn_help(_, caller)
    if verify_access(caller, config.enable_cmd_help) then
        command_help(caller)
    end
end

-- Commands definition calls start here
AddUserCommand("hk_help", cmd_data({"cmd_help", "?"}, serverfn_help))

AddUserCommand("hk_info", cmd_data({"info", "i"}, function(params, caller)
    if verify_access(caller, config.enable_cmd_info) then
        command_info(caller, merge_spaced_params(params))
    end
end, command_spaced_params, command_spaced_paramsoptional))

AddUserCommand("hk_portal", cmd_data({"portal", "q", "p", "esc"}, function(_, caller)
    if verify_access(caller, config.enable_cmd_return, true) then
        command_mark(caller)
    end
    if verify_access(caller, config.enable_cmd_portal) then
        command_portal(caller)
    end
end))

AddUserCommand("hk_revive", cmd_data({"revive", "r", "re", "lifehack"}, function(params, caller)
    local name = merge_spaced_params(params)
    if name ~= nil and name ~= "" then
        log_d("Running revive other command; target name == " .. name)
        if verify_access(caller, config.enable_cmd_revive_other) then
            command_revive(caller, "Remote revive command", name)
        end
    else
        log_d("Running revive self command")
        if verify_access(caller, config.enable_cmd_revive) then
            command_revive(caller, "Revive command")
        end
    end
end, command_spaced_params, command_spaced_paramsoptional))

local function cmd_data_simple(aliases, field, fn, args)
    return cmd_data(aliases, function(_, caller)
        if verify_access(caller, config[field]) then
            fn(caller, args)
        end
    end)
end

AddUserCommand("hk_die", cmd_data_simple({"die"}, "enable_cmd_die", command_kill))

AddUserCommand("hk_mark", cmd_data_simple({"mark", ","}, "enable_cmd_return", command_mark, true))

AddUserCommand("hk_back", cmd_data_simple({"back", "b", "return"}, "enable_cmd_return", command_return, true))

AddUserCommand("hk_save", cmd_data({"save", "+"}, function(params, caller)
    if verify_access(caller, config.enable_cmd_save) then
        command_fast_travel(caller, 0, merge_spaced_params(params))
    end
end, command_spaced_params, command_spaced_paramsoptional))

AddUserCommand("hk_load", cmd_data({"load", "=", "goto", "travel", "fasttravel"}, function(params, caller)
    if verify_access(caller, config.enable_cmd_save) then
        command_fast_travel(caller, 1, merge_spaced_params(params))
    end
end, command_spaced_params, command_spaced_paramsoptional))

AddUserCommand("hk_delete", cmd_data({"delete", "-", "remove", "forget"}, function(params, caller)
    if verify_access(caller, config.enable_cmd_save) then
        command_fast_travel(caller, 2, merge_spaced_params(params))
    end
end, command_spaced_params, command_spaced_paramsoptional))

AddUserCommand("hk_deleteall", cmd_data({"deleteall", "-all", "removeall", "forgetall"}, function(_, caller)
    if verify_access(caller, config.enable_cmd_save) then
        command_fast_travel(caller, 3, nil)
    end
end))

AddUserCommand("hk_extinguish", cmd_data({"extinguish", "e", "ext"}, function(_, caller)
    if verify_access(caller, config.enable_cmd_ext) then
        command_ext(caller)
    end
end))

AddUserCommand("hk_invite", cmd_data({"invite"}, function(params, caller)
    if verify_access(caller, config.enable_cmd_invite) then
        command_invite(caller, 0, merge_spaced_params(params))
    end
end, command_spaced_params, command_spaced_paramsoptional))

AddUserCommand("hk_join", cmd_data({"join"}, function(params, caller)
    if verify_access(caller, config.enable_cmd_invite) then
        command_invite(caller, 1, merge_spaced_params(params))
    end
end, command_spaced_params, command_spaced_paramsoptional))

AddUserCommand("hk_deny", cmd_data({"deny"}, function(params, caller)
    if verify_access(caller, config.enable_cmd_invite) then
        command_invite(caller, 2, merge_spaced_params(params))
    end
end, command_spaced_params, command_spaced_paramsoptional))

AddUserCommand("hk_forceddeny", cmd_data({"forceddeny", "deny!"}, function(params, caller)
    if verify_access(caller, config.enable_cmd_invite) then
        command_invite(caller, 3, merge_spaced_params(params))
    end
end, command_spaced_params, command_spaced_paramsoptional))

AddUserCommand("hk_denyall", cmd_data_simple({"denyall"}, "enable_cmd_invite", command_invite, 4))

AddUserCommand("hk_teleport", cmd_data({"teleport", "tp"}, function(params, caller)
    if verify_access(caller, config.enable_cmd_tp) then
        command_tp(caller, merge_spaced_params(params))
    end
end, command_spaced_params, command_spaced_paramsoptional))

AddUserCommand("hk_stasis", cmd_data_simple({"stasis", "hibernate"}, "enable_cmd_stasis", command_stasis))

AddUserCommand("hk_spawn", cmd_data({"spawn"}, function(params, caller)
    if params.name and verify_access(caller, config.enable_cmd_spawn) then
        command_spawn_prefab(caller, false, params.name, params.quantity, params.step, params.delay)
    end
end, {"name", "quantity", "step", "delay"}, {true, true, true, true}))

AddUserCommand("hk_give", cmd_data({"give"}, function(params, caller)
    if params.name and verify_access(caller, config.enable_cmd_spawn) then
        command_spawn_prefab(caller, true, params.name, params.quantity, params.step, params.delay)
    end
end, {"name", "quantity", "step", "delay"}, {true, true, true, true}))

AddUserCommand("hk_kill", cmd_data({"kill"}, function(params, caller)
    if verify_access(caller, config.enable_cmd_kill) then
        command_kill(caller, true, merge_spaced_params(params))
    end
end, command_spaced_params, command_spaced_paramsoptional))

AddUserCommand("hk_warp", cmd_data({"warp", "teleport_to"}, function(params, caller)
    if params.x and params.y and params.z and verify_access(caller, config.enable_cmd_warp) then
        command_teleport(caller, params.x, params.y, params.z)
    end
end, {"x", "y", "z"}, {true, true, true}))

AddUserCommand("hk_rank", cmd_data({"rank"}, function(params, caller)
    if verify_access(caller, config.enable_cmd_rank) then
        command_rank(caller, params.x, params.y, merge_spaced_params(params))
    end
end, merge_tables({"x", "y"}, command_spaced_params), merge_tables({true, true}, command_spaced_paramsoptional)))

AddUserCommand("hk_reviveall", cmd_data({"reviveall"}, function(_, caller)
    if verify_access(caller, config.enable_cmd_reviveall) then
        command_reviveall()
    end
end))

AddUserCommand("hk_resetall", cmd_data_simple({"resetall"}, "enable_cmd_resetall", command_reset, true))

AddUserCommand("hk_reset", cmd_data({"reset"}, function(params, caller)
    if verify_access(caller, config.enable_cmd_reset) then
        command_reset(caller, false, params.name)
    end
end, {"name"}, {true}))

AddUserCommand("hk_pull", cmd_data({"pull"}, function(params, caller)
    if verify_access(caller, config.enable_cmd_pull) then
        local name = merge_spaced_params(params)
        local player = find_player(caller, name)
        if player then
            command_mark(player, false, true)
        else
            say(player, "Could not find " .. name)
        end
        command_pull(caller, name)
    end
end, command_spaced_params, command_spaced_paramsoptional))

AddUserCommand("hk_push", cmd_data({"push"}, function(params, caller)
    if verify_access(caller, config.enable_cmd_pull) then
        local name = merge_spaced_params(params)
        local player = find_player(caller, name)
        command_pull(player, name, true)
    end
end, command_spaced_params, command_spaced_paramsoptional))

local function cmd_data_freeze(freeze)
    return function(params, caller)
        if verify_access(caller, config.enable_cmd_freeze) then
            command_freeze(caller, freeze, merge_spaced_params(params))
        end
    end
end

AddUserCommand("hk_freeze",
    cmd_data({"freeze", "@", "stop"}, cmd_data_freeze(true), command_spaced_params, command_spaced_paramsoptional))
AddUserCommand("hk_unfreeze",
    cmd_data({"unfreeze", "#", "release"}, cmd_data_freeze(false), command_spaced_params, command_spaced_paramsoptional))

AddUserCommand("hide", cmd_data({"cloak"}, function(params, caller)
    if verify_access(caller, config.enable_cmd_hide) then
        command_hide(caller, params.hide)
    end
end, {"hide"}, {true}))

AddUserCommand("hk_kick", cmd_data({"kick2"}, function(params, caller)
    if verify_access(caller, config.enable_cmd_kick) then
        command_kick(caller, merge_spaced_params(params))
    end
end, command_spaced_params, command_spaced_paramsoptional))

AddUserCommand("hk_drop", cmd_data({"drop"}, function(params, caller)
    if verify_access(caller, config.enable_cmd_drop) then
        command_drop(caller, merge_spaced_params(params))
    end
end, command_spaced_params, command_spaced_paramsoptional))

AddUserCommand("hk_file", cmd_data_simple({"file", "test", "check"}, "enable_cmd_filecheck", command_filecheck))

AddUserCommand("hk_tgm", cmd_data_simple({"tgm", "!"}, "enable_cmd_tgm", player_godmode, true))

AddUserCommand("hk_ban", cmd_data({"ban2"}, function(params, caller)
    if verify_access(caller, config.enable_cmd_ban) then
        command_ban(caller, merge_spaced_params(params), true)
    end
end, command_spaced_params, command_spaced_paramsoptional))

AddUserCommand("hk_unban", cmd_data({"unban2"}, function(params, caller)
    if verify_access(caller, config.enable_cmd_ban) then
        command_ban(caller, merge_spaced_params(params), false)
    end
end, command_spaced_params, command_spaced_paramsoptional))

function restore_stat(player, stats, name)
    if name then
        if verify_access(player, config.enable_cmd_revive_other) then
            player_restore_stat(player, stats, name)
        end
    else
        if verify_access(player, config.enable_cmd_tgm) then
            player_restore_stat(player, stats)
        end
    end
end

AddUserCommand("hk_heal", cmd_data({"heal", "restore", "cure"}, function(params, caller)
    restore_stat(caller, "health", merge_spaced_params(params))
end, command_spaced_params, command_spaced_paramsoptional))

AddUserCommand("hk_calm", cmd_data({"calm"}, function(params, caller)
    restore_stat(caller, "sanity", merge_spaced_params(params))
end, command_spaced_params, command_spaced_paramsoptional))

AddUserCommand("hk_feed", cmd_data({"feed"}, function(params, caller)
    restore_stat(caller, "hunger", merge_spaced_params(params))
end, command_spaced_params, command_spaced_paramsoptional))

AddUserCommand("hk_restoreall", cmd_data({"restoreall"}, function(params, caller)
    restore_stat(caller, "health sanity hunger", merge_spaced_params(params))
end, command_spaced_params, command_spaced_paramsoptional))

AddUserCommand("hk_repick_character", cmd_data({"repick_character"}, function(_, caller)
    if verify_access(caller, config.enable_cmd_repick_character) then
        inventory_drop_everything(caller)
        despawn_player(caller)
    end
end))

AddUserCommand("hk_alert", cmd_data({"alert", "announce"}, function(params, caller)
    if verify_access(caller, config.enable_cmd_alert) then
        announce(merge_spaced_params(params))
    end
end, command_spaced_params, command_spaced_paramsoptional))

AddUserCommand("hk_creative", cmd_data({"creative"}, function(params, caller)
    if verify_access(caller, config.enable_cmd_creative) then
        toggle_creative_mode(caller, merge_spaced_params_or_nil(params))
    end
end, command_spaced_params, command_spaced_paramsoptional))

AddUserCommand("hk_add_points", cmd_data({"add_points"}, function(params, caller)
    if verify_access(caller, config.enable_cmd_add_points) then
        add_points(caller, merge_spaced_params(params, true), params.param1)
    end
end, command_spaced_params, command_spaced_paramsoptional))
-- Commands definition calls ends here

if config.enable_rank_badges then
    -- inspired by: Moderator Commands [Fork]
    local ImageButton = require "widgets/imagebutton"
    local Text = require "widgets/text"

    function apply_ranks_badges(widgets)
        for i = 1, #widgets do
            local widget = widgets[i]
            if not widget.rankBadge then
                local player = read_player_rank(widget.userid)

                if player then
                    local rank = player.rank

                    if rank ~= "Admin" and rank ~= "User" then
                        widget.rankBadge = widget:AddChild(ImageButton("images/avatars.xml", "avatar_admin.tex",
                                                               "avatar_admin.tex", "avatar_admin.tex"))
                        widget.rankBadge:Disable()
                        widget.rankBadge:SetPosition(-355, -13, 0)
                        widget.rankBadge.scale_on_focus = false
                        widget.rankBadge.image:SetScale(.3)
                        widget.rankBadge.image:SetTint(1, 1, 1, 1)
                        widget.rankBadge:SetHoverText(rank, {
                            font = GLOBAL.NEWFONT_OUTLINE,
                            size = 24,
                            offset_x = 0,
                            offset_y = 30,
                            colour = GLOBAL.WHITE
                        })
                        widget.rankBadgeLabel = widget.rankBadge:AddChild(Text(GLOBAL.UIFONT, 20, "M", GLOBAL.GREY))
                        widget.rankBadgeLabel:SetPosition(0, -2, 0)
                        widget.rankBadge:Show()
                    end
                end
            end
        end
    end

    AddClassPostConstruct("screens/playerstatusscreen", function(self)
        local oldDoInit = self.DoInit
        self.DoInit = function(self, ClientObjs)
            oldDoInit(self, ClientObjs)
            apply_ranks_badges(self.player_widgets)
            -- inject into update function (called e.g. when scrolling)
            if self.scroll_list.updatefn and not self.scroll_list.old_updatefn_ModCmds then
                self.scroll_list.old_updatefn_ModCmds = self.scroll_list.updatefn
                self.scroll_list.updatefn = function(playerlisting, v, i)
                    self.scroll_list.old_updatefn_ModCmds(playerlisting, v, i)
                    if v then
                        local player = read_player_rank(playerlisting.userid)

                        if player and player.rank then
                            local rank = player.rank

                            if rank ~= "Admin" and rank ~= "User" and playerlisting.rankBadge then
                                playerlisting.rankBadge:SetHoverText(rank)
                                playerlisting.rankBadge:Show()
                            elseif playerlisting.rankBadge then
                                playerlisting.rankBadge:Hide()
                            end
                        end
                    elseif playerlisting.rankBadge then
                        playerlisting.rankBadge:Hide()
                    end
                end
            end
        end
    end)

    local function serialize_players_ranks_table(list)
        return json.encode(list)
    end

    local function generate_players_ranks_table()
        local isdedicated = not GLOBAL.TheNet:GetServerIsClientHosted()
        local players_table = GLOBAL.TheNet:GetClientTable()
        local tbl = {}
        for _, player in ipairs(players_table) do
            if not isdedicated or player.performance == nil then
                local userid = player.userid
                local rank = "User"
                if is_admin(player) then
                    rank = "Admin"
                elseif is_player_rank(player, rank_names[3]) then
                    rank = "Moderator"
                elseif is_player_rank(player, "elitemod") or is_player_rank(player, "trusted") then
                    rank = "Senior Moderator"
                end

                tbl[userid] = {
                    rank = rank
                }
            end
        end

        return tbl
    end

    function regenerate_players_ranks_json()
        local list = generate_players_ranks_table()
        local str = serialize_players_ranks_table(list)

        if GLOBAL.TheWorld and GLOBAL.TheWorld.net and GLOBAL.TheWorld.net.hk_players_ranks then
            GLOBAL.TheWorld.net.hk_players_ranks:set(str)
        end
    end

    local function update_player_ranks_client_method(inst)
        inst.hk_players_ranks_json = inst.hk_players_ranks:value()
        local tbl = deserialize_json(inst.hk_players_ranks_json, "players ranks")
        if tbl ~= nil then
            inst.hk_players_ranks_table = tbl
        end
    end

    local function NetworkPostInit(inst)
        inst.hk_players_ranks = GLOBAL.net_string(inst.GUID, "hkranksdata.ranks", "hkranksdata.ranksdirty")
        inst.hk_players_ranks_json = ""
        inst.hk_players_ranks_table = nil
        if not GLOBAL.TheNet:IsDedicated() then
            inst:ListenForEvent("hkranksdata.ranksdirty", update_player_ranks_client_method)
        end
        if GLOBAL.TheNet:GetIsServer() then
            -- GLOBAL.TheWorld:ListenForEvent("ms_playerjoined", function(inst, data) --handled in on_player_join.lua!
            -- 	regenerate_players_ranks_json()
            -- end)
            GLOBAL.TheWorld:ListenForEvent("ms_playercounts", function(_, _)
                regenerate_players_ranks_json()
            end)
            --[[
    GLOBAL.TheWorld:ListenForEvent("ms_playerleft", function(inst, data)
        regenerate_players_ranks_json()
    end)
--]]
        end
    end

    AddPrefabPostInit("forest_network", NetworkPostInit)
    AddPrefabPostInit("cave_network", NetworkPostInit)

    function read_player_rank(userid)
        local table_userid_rankdisplayname = GLOBAL.TheWorld and GLOBAL.TheWorld.net and
                                                 GLOBAL.TheWorld.net.hk_players_ranks_table
        if table_userid_rankdisplayname == nil or type(table_userid_rankdisplayname) ~= "table" then
            return nil
        end

        return table_userid_rankdisplayname[userid]
    end
end