if config.stack_size ~= 0 then
    local stackable_replica = require "components/stackable_replica"
    --local net_byte = _G.net_byte
    --local net_ushortint = _G.net_ushortint
    _TUNING.STACK_SIZE_LARGEITEM = config.stack_size
    _TUNING.STACK_SIZE_MEDITEM = config.stack_size
    _TUNING.STACK_SIZE_SMALLITEM = config.stack_size
    function stackable_replica._ctor(self, inst)
        self.inst = inst
        if config.stack_size > 250 then
            self._stacksize = _G.net_ushortint(inst.GUID, "stackable._stacksize", "stacksizedirty") --max 65535
        else
            self._stacksize = _G.net_byte(inst.GUID, "stackable._stacksize", "stacksizedirty") -- max 255
        end
        self._maxsize = config.stack_size
    end

    function stackable_replica:SetMaxSize(_)
        self._maxsize = config.stack_size
    end

    function stackable_replica:MaxSize()
        return self._maxsize
    end
end