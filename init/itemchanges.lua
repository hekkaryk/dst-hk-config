if config.orangeamulet_range ~= 4 then
    _TUNING.ORANGEAMULET_RANGE = config.orangeamulet_range
end
if config.orangeamulet_icd ~= 0.33 then
    _TUNING.ORANGEAMULET_ICD = config.orangeamulet_icd
end
--100k is too much, 10k load but turns game into slideshow

if config.ratio_amulets ~= 0 or config.orangeamulet_pick_stacks then
    local function new_pickup(inst, owner)
        if owner == nil or owner.components.inventory == nil then
            return
        end
        local x, y, z = owner.Transform:GetWorldPosition()
        local ents =
            _G.TheSim:FindEntities(
            x,
            y,
            z,
            _TUNING.ORANGEAMULET_RANGE,
            {"_inventoryitem"},
            {"INLIMBO", "NOCLICK", "catchable", "fire", "minesprung", "mineactive"}
        )
        for i, v in ipairs(ents) do
            if
                v.components.inventoryitem ~= nil and v.components.inventoryitem.canbepickedup and
                    v.components.inventoryitem.cangoincontainer and
                    not v.components.inventoryitem:IsHeld() and
                    owner.components.inventory:CanAcceptCount(v, 1) > 0
             then
                local v_pos = v:GetPosition()

                local maxaccepted = 0
                local inventory_accepted = false
                if config.orangeamulet_pick_stacks then
                    if owner.components.inventory:CanAcceptCount(v) then
                        inventory_accepted = true
                    end
                    if v.components.stackable ~= nil then
                        maxaccepted = owner.components.inventory:CanAcceptCount(v)
                        if inst.components.finiteuses then
                            maxaccepted = math.min(maxaccepted, inst.components.finiteuses.current)
                        end
                        v = v.components.stackable:Get(maxaccepted)
                    end
                else
                    inventory_accepted = true
                    maxaccepted = 1
                    if v.components.stackable ~= nil then
                        v = v.components.stackable:Get()
                    end
                end
                if inventory_accepted and inst.components.finiteuses then
                    inst.components.finiteuses:Use(maxaccepted)
                    _G.SpawnPrefab("sand_puff").Transform:SetPosition(v.Transform:GetWorldPosition())
                end

                if v.components.trap ~= nil and v.components.trap:IsSprung() then
                    v.components.trap:Harvest(owner)
                else
                    owner.components.inventory:GiveItem(v, nil, v_pos)
                end
                return
            end
        end
    end

    local function new_onequip_orange(inst, owner)
        owner.AnimState:OverrideSymbol("swap_body", "torso_amulets", "orangeamulet")
        inst.task = inst:DoPeriodicTask(_TUNING.ORANGEAMULET_ICD, new_pickup, nil, owner)
    end

    AddPrefabPostInit(
        "orangeamulet",
        function(inst)
            if ismastersim() and inst and inst.components and inst.components.equippable then
                inst.components.equippable:SetOnEquip(new_onequip_orange)
            end
        end
    )
end
