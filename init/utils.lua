-- returns true on server
function ismastersim()
    return _G.TheWorld.ismastersim == true
end

-- Some utility functions
function is_cave()
    return _G.TheWorld:HasTag("cave")
end

function announce(message)
    _G.TheNet:Announce(message)
end

function s_if_plural(x)
    return x > 1 and "s" or ""
end

function is_owned_inventoryitem(inst)
    if inst and inst.components then
        return inst.components.inventoryitem ~= nil and inst.components.inventoryitem.owner ~= nil
    else
        log_w("Error in is_owned_inventoryitem function!")
    end
end

function file_check(file_name)
    local file = io.open(file_name, "r")
    if file == nil then
        return false
    else
        file:close()
        return true
    end
end

function file_create(file_name)
    local file = io.open(file_name, "w")
    if file then
        file:close()
        return true
    else
        return false
    end
end

function log_event(type, message)
    if config.datetimeformat ~= "none" then
        local now = nil
        local datetimeformat = config.datetimeformat
        local utc_string = " UTC-" .. config.timezone
        local datetime = ""
        if config.timezone ~= -13 then
            now = os.time() + config.timezone * 60 * 60
            datetimeformat = "!" .. datetimeformat
            datetime = os.date("!" .. datetimeformat, now) .. utc_string
        else
            now = os.time()
            local ut = os.date("!*t", now)
            local lt = os.date("*t", now)
            local hour
            local min
            if ut.day < lt.day or ut.month < lt.month or ut.year < lt.year then
                hour = 24 - ut.hour + lt.hour
                min = 60 - ut.min + ut.min
            else
                hour = lt.hour - ut.hour
                min = lt.min - ut.min
            end
            local sign = (hour < 0 or (min < 0 and hour == 0)) and "" or "+"
            min = 0 and "" or "." .. min
            utc_string = " Local time (UTC" .. sign .. hour .. min .. ")"
            datetime = os.date(datetimeformat, now) .. utc_string
        end
        print(datetime .. " [Hk]Config++ " .. type .. ": " .. message)
    else
        print("[Hk]Config++ " .. type .. ": " .. message)
    end
end

function log_system_total_failure(msg)
    print("[Hk]Config++ Fatal system error: " .. (msg or "NIL"))
end

function log_f(msg)
    if config.log_level >= 1 then
        log_event("Fatal", msg)
    end
end

function log_e(msg)
    if config.log_level >= 2 then
        log_event("Error", msg)
    end
end

function log_w(msg)
    if config.log_level >= 3 then
        log_event("Warning", msg)
    end
end

function log_i(msg)
    if config.log_level >= 4 then
        log_event("Info", msg)
    end
end

function log_l(msg)
    if config.log_level >= 5 then
        log_event("Log", msg)
    end
end

function log_d(msg)
    if config.log_level >= 6 then
        log_event("Debug", msg)
    end
end

-- makes player character speak out loud
function say(player, message)
    if player and player.name and message then
        print(player.name .. ": " .. message)
        if player.components and player.components.talker then
            player.components.talker:Say(message, nil, true)
        end
    end
end

function enable(name)
    if config["enable_" .. name] then
        modimport("init/" .. name)
    end
end

function despawn_player(player)
    log_i("Despawning player " .. player.name)
    _G.TheWorld:PushEvent("ms_playerdespawnanddelete", player)
end

function inventory_drop_everything(target)
    if target and target.components and target.components.inventory then
        if target.components.inventory.activeitem ~= nil then
            target.components.inventory:DropItem(target.components.inventory.activeitem)
            target.components.inventory:SetActiveItem(nil)
        end

        for k = 1, target.components.inventory.maxslots do
            local v = target.components.inventory.itemslots[k]
            if v ~= nil then
                target.components.inventory:DropItem(v, true, true)
            end
        end

        for _, v in pairs(target.components.inventory.equipslots) do
            target.components.inventory:DropItem(v, true, true)
        end
    end
end
-- function string:split(inSplitPattern, outResults)
--     if not outResults then
--         outResults = {}
--     end
--     local theStart = 1
--     local theSplitStart, theSplitEnd = string.find(self, inSplitPattern, theStart)
--     while theSplitStart do
--         table.insert(outResults, string.sub(self, theStart, theSplitStart - 1))
--         theStart = theSplitEnd + 1
--         theSplitStart, theSplitEnd = string.find(self, inSplitPattern, theStart)
--     end
--     table.insert(outResults, string.sub(self, theStart))
--     return outResults
-- end

function get_random_char()
    local number = math.random(48, 83)
    if number > 57 then
        number = number + 7
    end
    return string.char(number)
end

function get_long_random_string()
    local result = ""
    for i = 1, 50 do
        result = result .. get_random_char()
        if i % 10 == 0 and i < 50 then
            result = result .. "-"
        end
    end
    return result
end

function merge_tables(first, second)
    local orig_size = #first
    for i = 1, #second do
        first[orig_size + i] = second[i]
    end
    return first
end

function deserialize_json(value, source)
    local result = nil

    local res, error =
        GLOBAL.pcall( -- return true|false + any error/value X sent in ```error(x)```
        function()
            result = GLOBAL.json.decode(value)
        end
    )

    return result, error
end

function cast_to_string(value, oneline)
    local result = ""

    if type(value) == "table" then
        for _, v in pairs(value) do
            _v = ""
            if type(value) == "table" then
                _v = cast_to_string(v)
            elseif v == true then
                _v = "true"
            elseif v == false then
                _v = "false"
            else
                _v = v
            end
            result = result .. _v

            if oneline ~= true then
                result = result .. "\n"
            end
        end
    else
        result = value
    end

    return result
end

-- Splits string by [ ,;] (space, comma and semicolon)
function first_level_string_split(value)
    return string.gmatch(value, "([^,;%s]+)")
end

-- Splits string by [:=] (colon and equals sign)
function second_level_string_split(value)
    return string.gmatch(value, "([^:=]+)")
end

function nil_or_empty(value)
    return value == nil or value == ""
end

function not_nil_or_empty(value)
    return not nil_or_empty(value)
end

function table_contains(table, value)
    for _,v in pairs(table) do
        if v == value then return true end
    end

    return false
end